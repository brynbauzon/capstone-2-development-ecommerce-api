const User = require('./../models/users');
const Product = require('./../models/products');

const auth = require('./../auth');

const bcrypt = require('bcrypt');

module.exports.isAdmin = (userId, reqBody) => {
	// console.log(userId)
	// console.log(reqBody)

	return User.findByIdAndUpdate(userId, reqBody, {new:true}).then( result => {
		// console.log(result)


		return result
	})

}


module.exports.getAllUsers = () => {
	//difference between findOne() & find()
			//findOne() returns one document
			//find() returns an array of documents []

	return User.find().then( (result, error) => {
		if(result !== null){
			return result
		} else {
			return error
		}
	})
}



module.exports.register = (reqBody) => {
	

	let newUser = new User({

		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	//save the newUser object to  the database
	return newUser.save().then( (result, error) => {
		//console.log(result)	//document
		if(result){
			return true
		} else {
			return error
		}
	})
}

module.exports.login = (reqBody) => {
	const {email, password} = reqBody

	return User.findOne({email: email}).then( (result, error) => {
		// console.log(result)

		if(result == null){
			console.log('email null');
			return false
		} else {

			let isPwCorrect = bcrypt.compareSync(password, result.password)	//boolean bec it compares two arguments

			if(isPwCorrect == true){
			//return json web token
				//invoke the function which creates the token upon logging in
				// requirements in creating a token:
					// if password matches from existing pw from db
					return {access: auth.createAccessToken(result)}
					// return auth.createAccessToken(result)
			} else {
				return false
			}
		}
	})
}


module.exports.getMyOrders = (userData) => {
        
        

           return   User.findById(userData.id).then((result, error) =>{

              		if(result !== null){
						return result
					} else {
						return error
					}

              })

          

        
   }


   module.exports.getAllOrders = (userData) => {
      
            return  User.find({ orders: { $exists: true, $ne: [] } }).select()
            let allActiveOrders = []

            for (let i = 0; i < allOrders.length; i++) {
                if (allOrders[i].orders.length >= 1) {
                    allActiveOrders.push(allOrders)
                }
            }

            
    }



////Checkout function Code 1
 

// module.exports.checkout = (userId, cart) => {
// 	//console.log(cart.products[0].productName);

// 	return User.findById(userId).then(user => {
// 		if(user === null){
// 			return false;
// 		} else {
// 			user.orders.push(
// 				{
// 					products: cart.products,
// 					totalAmount: cart.totalAmount
// 				}
// 			);
// 			//console.log(user.orders);
// 			return user.save().then((updatedUser, error) => {
// 				if(error){
// 					return false;
// 				} else {
// 					const currentOrder = updatedUser.orders[updatedUser.orders.length-1];
// 					//console.log(currentOrder)
// 					currentOrder.products.forEach(product => {
// 						Product.findById(product.productId)
// 							.then(foundProduct => {
// 								foundProduct.orders.push({orderId:currentOrder._id})
// 								foundProduct.save()
// 						})
// 					})
// 					return true;
// 				}
// 			})
// 		}
// 	})
// }


//Check out code 2
module.exports.checkout = async (userData, cart) => {
        
       

            //save newOrder from user to database
            const newOrder = await User.findByIdAndUpdate(userData.id, { $addToSet: { orders: cart } }, { new: true })

            //get latest order from user
            const latestOrder = newOrder.orders[newOrder.orders.length - 1]
            const orderId = latestOrder._id

            let productId
            let productDetail
            for (let i = 0; i < cart.products.length; i++) {
                //find Product using product ID from cart.products[0].productId
                productId = cart.products[i].productId
                //save latest order id from user to Product.orders Array
                productDetail = await Product.findByIdAndUpdate(productId,
                    { $addToSet: { orders: [{ orderId }] } }, { new: true })
            }

            
                           
     if(productDetail && latestOrder){
		return true
	} else {
		return false
	}
       
}