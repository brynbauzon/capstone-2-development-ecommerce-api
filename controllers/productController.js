const Product = require('./../models/products');
const User = require('./../models/users');

const auth = require('./../auth');

const bcrypt = require('bcrypt');


module.exports.getAllProduct = () => {
	//difference between findOne() & find()
			//findOne() returns one document
			//find() returns an array of documents []

	return Product.find().then( (result, error) => {
		if(result !== null){
			return result
		} else {
			return error
		}
	})
}


module.exports.getAllActive = (reqBody) =>{

return  Product.find({isActive: true}).then((result, error)=>{
 
    if(result){

        	return result
        }else{

        	return error
        }


 });     
       
};

module.exports.addProduct= (reqBody) =>{

 
  	let newProduct = new Product({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	
			
	});

    return newProduct.save().then( (result, error) => {
		//console.log(result)	//document
		if(result){
			return true
		} else {
			return error
		}
	})   
      

};


module.exports.getProduct = (reqBody) =>{

console.log('Im User')
console.log(reqBody)

return Product.findOne({_id: reqBody}).then((result, error) =>{


	if(result == null){

		return 'Product not found'
	}else{

		if(result){

			return result
		}else{

			return error
		}

	}
 }) 

}	


module.exports.getProductAdmin = (reqBody) =>{
//console.log(reqBody)

return Product.findOne({_id: reqBody}).then((result, error) =>{

	if(result == null){

		return 'Product not found'
	}else{

		if(result){

			console.log()
			return result
		}else{

			return error
		}

	}
 }) 

}


module.exports.updateProduct = (productId, reqBody) => {
	// console.log(productId)
	// console.log(reqBody)

	const {name, description, price} = reqBody

	const updateProduct = {
		name: name,
		description: description,
		price: price
	}

return Product.findByIdAndUpdate(productId, updateProduct, {new:true}).then((result, error) => {
	
	if (error){
        console.log(error)
	}
		    else{
		console.log("Product Edited : ", result);
}
	})

}


module.exports.archiveProduct = (productId) =>{



 const isActive = {isActive:'false'}
 return Product.findByIdAndUpdate(productId, isActive, {new:true}).then((result, error) => {
	
		if (error){
        console.log(error)
    }
    else{
        console.log("Archive Product : ", result);
    }
 })

}



module.exports.activateProduct = (productId) =>{



 const isActive = {isActive:'true'}
 return Product.findByIdAndUpdate(productId, isActive, {new:true}).then((result, error) => {
	
		if (error){
        console.log(error)
    }
    else{
        console.log("Product is now Activated : ", result);
    }
 })

}

