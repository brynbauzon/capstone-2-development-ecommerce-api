const express = require('express');
//Create routes

//Router() handles the requests
const router = express.Router();

//User model 
// const User = require('./../models/User');

const auth = require('./../auth');

//User Controller
const productController = require('./../controllers/productController');

//Get all Products
router.get('/all', (req, res) => {

productController.getAllProduct().then(resultFromController => res.send(resultFromController))


})

//Get all active products
router.get('/active', (req, res) => {

productController.getAllActive().then(resultFromController => res.send(resultFromController))
})


//Add Products (Admin Only)
router.post("/", auth.verify, (req, res) => {

   const checkAdmin = auth.decode(req.headers.authorization).isAdmin

    if(checkAdmin === true){

         productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))

    }else{

        return res.send(false)
    }

});


//Get specific product 
router.get('/:productId', (req, res)=>{


     const checkAdmin = auth.decode(req.headers.authorization).isAdmin


     if(checkAdmin === true){

          productController.getProductAdmin(req.params.productId).then(resultFromController => res.send(resultFromController))

    }else{

         productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
    }  
});




//Update Product
router.put('/:productId', auth.verify, (req, res)=>{
//if isAdmin is equals to false
//res.send(fasle)

//if isAdmin is equals to true
productController.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));

});

//Archive Product //ongoing
//set isActive to False

router.put('/:productId/archive', auth.verify, (req, res)=> { 
//if isAdmin is equals to false
//res.send(false)

//if isAdmin is equals to true


const checkAdmin = auth.decode(req.headers.authorization).isAdmin
 
 let productId = req.params.productId
 
    if(checkAdmin === true){

         productController.archiveProduct(productId).then(resultFromController => res.send(resultFromController))

    }else{

        return res.send(false)
    }

});



//Unarchive Product 
//set isActive to false
router.put('/:productId/activate', auth.verify, (req, res)=>{
//if isAdmin is eqauls to false
//res.send(false)
 
 const checkAdmin = auth.decode(req.headers.authorization).isAdmin
 
 let productId = req.params.productId
 
    if(checkAdmin === true){

         productController.activateProduct(productId).then(resultFromController => res.send(resultFromController))

    }else{

        return res.send(false)
    }

//if isAdmin is equals to true

})



module.exports = router;