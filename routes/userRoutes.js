const express = require('express');
//Create routes

//
const auth = require('./../auth');
//Router() handles the requests
const router = express.Router();

//User model 
// const User = require('./../models/User');

//User Controller
const userController = require('./../controllers/userController');

//Show all user
router.get('/', (req, res) => {
	userController.getAllUsers().then(result => res.send(result))
});

//Setting admin
// router.post('/:userId', (req, res) => {
// 	//expecting data/object coming from request body

// 	userController.isAdmin(req.params.userId).then(result => res.send(result))
// })


//register route
router.post('/register', (req, res) => {
	//expecting data/object coming from request body

	userController.register(req.body).then(result => res.send(result))
})


router.post('/:id/isadmin', (req, res) => {
    //expecting data/object coming from request body

    userController.isAdmin(req.params.id, req.body).then(result => res.send(result))
})

router.post('/login', (req, res) => {
	userController.login(req.body).then( result => res.send(result))
})


//Checkout
router.post("/checkout", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization)
    let checkAdmin = auth.decode(req.headers.authorization).isAdmin

    if(checkAdmin === false){

        userController.checkout(userData, req.body)
            .then(result => {
                res.send(result.latestOrder)
            });
    }else{

        return 'Admin is not allowed for checkout'
    }
})

//My-orders
router.get("/my-orders", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization)
    let checkAdmin = auth.decode(req.headers.authorization).isAdmin

    if(checkAdmin === false){
    userController.getMyOrders(userData)
        .then(result => {
            res.send(result)
        });

    }else{

        return 'For user orders view'
    }
})

//View all the orders Admin Only
router.get("/orders", auth.verify, (req, res) => {


    let userData = auth.decode(req.headers.authorization)
    let checkAdmin = auth.decode(req.headers.authorization).isAdmin

    if(checkAdmin === true){

    userController.getAllOrders(userData)
        .then(result => {
            res.send(result)
        });
    }else{

        return 'For admin orders view'
    }  
})

module.exports = router;